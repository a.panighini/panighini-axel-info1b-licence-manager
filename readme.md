Module 104 Exercice du 2021.06.21
---

Bonjour Monsieur Maccaud,

J'espère que vous aller bien, pour ma part ça allait plutot bien malgrès avoir passé mon week-end dans le noir et l'incertitude.
Maintenant j'ai enfin la certitude que mon projet ne fonctionne pas entièrement, j'y ai tout de même mis tout mon coeur et tout mon week-end
pour essayer de le mener à bien.
J'espère que cela suffira à satisfaire vos attentes.

Meilleures salutations
Panighini Axel de la classe info1b
