"""
    Fichier : gestion_licences_wtf_forms.py
    Auteur : OM 2021.03.22
    Gestion des formulaires avec WTF
"""
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms import SubmitField
from wtforms.validators import Length
from wtforms.validators import Regexp


class FormWTFAjouterlicences(FlaskForm):
    """
        Dans le formulaire "licences_ajouter_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    categorie_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    categorie = StringField("Clavioter la dénomination de la catégorie ", validators=[Length(min=2, max=20, message="min 2 max 20"),
                                                                   Regexp(categorie_regexp,
                                                                          message="Pas de chiffres, de "
                                                                                  "caractères "
                                                                                  "spéciaux, "
                                                                                  "d'espace à double, de double "
                                                                                  "apostrophe, de double trait "
                                                                                  "union")
                                                                   ])
    submit = SubmitField("Enregistrer catégorie")


class FormWTFUpdatelicence(FlaskForm):
    """
        Dans le formulaire "licences_update_wtf.html" on impose que le champ soit rempli.
        Définition d'un "bouton" submit avec un libellé personnalisé.
    """
    id_soft_cat_update_regexp = "^([A-Z]|[a-zÀ-ÖØ-öø-ÿ])[A-Za-zÀ-ÖØ-öø-ÿ]*['\- ]?[A-Za-zÀ-ÖØ-öø-ÿ]+$"
    id_soft_cat_update = StringField("Clavioter le nouveau prix de renouvellement de la licence ", validators=[Length(min=1, max=20, message="min 2 max 20"),
                                                                          Regexp(id_soft_cat_update_regexp,
                                                                                 message="Pas de chiffres, de "
                                                                                         "caractères "
                                                                                         "spéciaux, "
                                                                                         "d'espace à double, de double "
                                                                                         "apostrophe, de double trait "
                                                                                         "union")
                                                                          ])
    submit = SubmitField("Modifier la dénomination de la catégorie")


class FormWTFDeletelicence(FlaskForm):
    """
        Dans le formulaire "licences_delete_wtf.html"

        categorie : Champ qui reçoit la valeur du licence, lecture seule. (readonly=true)
        submit_btn_del : Bouton d'effacement "DEFINITIF".
        submit_btn_conf_del : Bouton de confirmation pour effacer un "licence".
        submit_btn_annuler : Bouton qui permet d'afficher la table "t_info_payment".
    """
    categorie = StringField("Effacer cette catégorie")
    submit_btn_del = SubmitField("Effacer catégorie")
    submit_btn_conf_del = SubmitField("Etes-vous sur d'effacer ?")
    submit_btn_annuler = SubmitField("Annuler")
