"""
    Fichier : gestion_licences_crud.py
    Auteur : OM 2021.03.16
    Gestions des "routes" FLASK et des données pour les licences.
"""
import sys

import pymysql
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from licence_Manager import obj_mon_application
from licence_Manager.database.connect_db_context_manager import MaBaseDeDonnee
from licence_Manager.erreurs.exceptions import *
from licence_Manager.erreurs.msg_erreurs import *
from licence_Manager.licences.gestion_licences_wtf_forms import FormWTFAjouterlicences
from licence_Manager.licences.gestion_licences_wtf_forms import FormWTFDeletelicence
from licence_Manager.licences.gestion_licences_wtf_forms import FormWTFUpdatelicence

# BTN_DELETE_HTML_ = request.values['id_soft_cat_btn_delete_html']

# PAYMENT_BTN_DELETE_HTML_ = request.values['id_soft_cat_btn_delete_html']

"""
    Auteur : OM 2021.03.16
    Définition d'une "route" /licences_afficher
    
    Test : ex : http://127.0.0.1:5005/licences_afficher
    
    Paramètres : order_by : ASC : Ascendant, DESC : Descendant
                id_soft_cat_sel = 0 >> tous les licences.
                id_soft_cat_sel = "n" affiche le licence dont l'id est "n"
"""


@obj_mon_application.route("/licences_afficher/<string:order_by>/<int:id_soft_cat_sel>", methods=['GET', 'POST'])
def licences_afficher(order_by, id_soft_cat_sel):
    if request.method == "GET":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion licences ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionlicences {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            with MaBaseDeDonnee().connexion_bd.cursor() as mc_afficher:
                if order_by == "ASC" and id_soft_cat_sel == 0:
                    strsql_categorie_afficher = """Select id_soft_cat, categorie FROM t_soft_cat 
                    ORDER BY id_soft_cat ASC """
                    mc_afficher.execute(strsql_categorie_afficher)
                elif order_by == "DESC" and id_soft_cat_sel == 0:
                    strsql_categorie_afficher = """Select id_soft_cat, categorie FROM t_soft_cat 
                    ORDER BY id_soft_cat DESC """
                    mc_afficher.execute(strsql_categorie_afficher)

                elif id_soft_cat_sel != 0:
                    valeur_id_soft_cat_selected_dictionnaire = {"value_id_soft_cat_selected": id_soft_cat_sel}
                    strsql_categorie_afficher = """Select id_soft_cat, categorie FROM t_soft_cat
                    AND id_soft_cat = %(value_id_soft_cat_selected)s """

                    mc_afficher.execute(strsql_categorie_afficher, valeur_id_soft_cat_selected_dictionnaire)

                else:
                    strsql_categorie_afficher = """Select id_soft_cat, categorie FROM t_soft_cat 
                                        ORDER BY id_soft_cat ASC """
                    mc_afficher.execute(strsql_categorie_afficher)

                data_licences = mc_afficher.fetchall()

                print("data_licences ", data_licences, " Type : ", type(data_licences))

                # Différencier les messages si la table est vide.
                if not data_licences and id_soft_cat_sel == 0:
                    flash("""La table "t_soft_cat" est vide. !!""", "warning")
                elif not data_licences and id_soft_cat_sel > 0:
                    # Si l'utilisateur change l'id_soft_cat dans l'URL et que le licence n'existe pas,
                    flash(f"La catégorie demandée n'existe pas !!", "warning")
                else:
                    # Dans tous les autres cas, c'est que la table "t_soft_cat" est vide.
                    # OM 2020.04.09 La ligne ci-dessous permet de donner un sentiment rassurant aux utilisateurs.
                    flash(f"Données catégories affichées !!", "success")

        except Exception as erreur:
            print(f"RGG Erreur générale. licences_afficher")
            # OM 2020.04.09 On dérive "Exception" par le "@obj_mon_application.errorhandler(404)"
            # fichier "run_mon_app.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            flash(f"RGG Exception {erreur} licences_afficher", "danger")
            raise Exception(f"RGG Erreur générale. {erreur}")
            # raise MaBdErreurOperation(f"RGG Exception {msg_erreurs['ErreurNomBD']['message']} {erreur}")

    # Envoie la page "HTML" au serveur.
    return render_template("licences/licences_afficher.html", data=data_licences)


"""
    Auteur : OM 2021.03.22
    Définition d'une "route" /licences_ajouter
    
    Test : ex : http://127.0.0.1:5005/licences_ajouter
    
    Paramètres : sans
    
    But : Ajouter un licence pour un film
    
    Remarque :  Dans le champ "name_licence_html" du formulaire "licences/licences_ajouter.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/licences_ajouter", methods=['GET', 'POST'])
def licences_ajouter_wtf():
    form = FormWTFAjouterlicences()
    if request.method == "POST":
        try:
            try:
                # Renvoie une erreur si la connexion est perdue.
                MaBaseDeDonnee().connexion_bd.ping(False)
            except Exception as erreur:
                flash(f"Dans Gestion licences ...terrible erreur, il faut connecter une base de donnée", "danger")
                print(f"Exception grave Classe constructeur Gestionlicences {erreur.args[0]}")
                raise MaBdErreurConnexion(f"{msg_erreurs['ErreurConnexionBD']['message']} {erreur.args[0]}")

            if form.validate_on_submit():
                name_licence_wtf = form.categorie.data

                name_licence = name_licence_wtf.lower()
                valeurs_insertion_dictionnaire = {"value_categorie": name_licence}
                print("valeurs_insertion_dictionnaire ", valeurs_insertion_dictionnaire)

                strsql_insert_soft_cat = """INSERT INTO t_soft_cat (id_soft_cat,categorie) VALUES (
                NULL,%(value_categorie)s) """
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(strsql_insert_soft_cat, valeurs_insertion_dictionnaire)

                flash(f"Données insérées !!", "success")
                print(f"Données insérées !!")

                # Pour afficher et constater l'insertion de la valeur, on affiche en ordre inverse. (DESC)
                return redirect(url_for('licences_afficher', order_by='ASC', id_soft_cat_sel=0))

        # ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except pymysql.err.IntegrityError as erreur_licence_doublon:
            # Dérive "pymysql.err.IntegrityError" dans "MaBdErreurDoublon" fichier "erreurs/exceptions.py"
            # Ainsi on peut avoir un message d'erreur personnalisé.
            code, msg = erreur_licence_doublon.args

            flash(f"{error_codes.get(code, msg)} ", "warning")

        # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
        except (pymysql.err.OperationalError,
                pymysql.ProgrammingError,
                pymysql.InternalError,
                TypeError) as erreur_gest_genr_crud:
            code, msg = erreur_gest_genr_crud.args

            flash(f"{error_codes.get(code, msg)} ", "danger")
            flash(f"Erreur dans Gestion licences CRUD : {sys.exc_info()[0]} "
                  f"{erreur_gest_genr_crud.args[0]} , "
                  f"{erreur_gest_genr_crud}", "danger")

    return render_template("licences/licences_ajouter_wtf.html", form=form)


"""
    Auteur : OM 2021.03.29
    Définition d'une "route" /licence_update
    
    Test : ex cliquer sur le menu "licences" puis cliquer sur le bouton "EDIT" d'un "licence"
    
    Paramètres : sans
    
    But : Editer(update) un licence qui a été sélectionné dans le formulaire "licences_afficher.html"
    
    Remarque :  Dans le champ "categorie" du formulaire "licences/licences_update_wtf.html",
                le contrôle de la saisie s'effectue ici en Python.
                On transforme la saisie en minuscules.
                On ne doit pas accepter des valeurs vides, des valeurs avec des chiffres,
                des valeurs avec des caractères qui ne sont pas des lettres.
                Pour comprendre [A-Za-zÀ-ÖØ-öø-ÿ] il faut se reporter à la table ASCII https://www.ascii-code.com/
                Accepte le trait d'union ou l'apostrophe, et l'espace entre deux mots, mais pas plus d'une occurence.
"""


@obj_mon_application.route("/licence_update", methods=['GET', 'POST'])
def licence_update_wtf():
    # L'utilisateur vient de cliquer sur le bouton "EDIT". Récupère la valeur de "id_soft_cat"
    id_soft_cat_update = request.values['id_soft_cat_btn_edit_html']

    # Objet formulaire pour l'UPDATE
    form_update = FormWTFUpdatelicence()
    try:
        print(" on submit ", form_update.validate_on_submit())
        if form_update.validate_on_submit():
            # Récupèrer la valeur du champ depuis "licences_update_wtf.html" après avoir cliqué sur "SUBMIT".
            # Puis la convertir en lettres minuscules.
            id_soft_cat_update_wtf = form_update.id_soft_cat_update.data
            id_soft_cat_update = id_soft_cat_update_wtf.lower()

            valeur_update_dictionnaire = {"value_id_soft_cat": id_soft_cat_update, "value_id_soft_cat": id_soft_cat_update}
            print("valeur_update_dictionnaire ", valeur_update_dictionnaire)

            str_sql_update_intitulelicence = """UPDATE t_soft_cat SET categorie = %(value_id_soft_cat)s WHERE 
            id_soft_cat = %(value_id_soft_cat)s """
            with MaBaseDeDonnee() as mconn_bd:
                mconn_bd.mabd_execute(str_sql_update_intitulelicence, valeur_update_dictionnaire)

            flash(f"Donnée mise à jour !!", "success")
            print(f"Donnée mise à jour !!")

            # afficher et constater que la donnée est mise à jour.
            # Affiche seulement la valeur modifiée, "ASC" et l'"id_soft_cat"
            return redirect(url_for('licences_afficher', order_by="ASC", id_soft_cat_sel=0))
        elif request.method == "GET":
            # Opération sur la BD pour récupérer "id_soft_cat" et "categorie" de la "t_soft_cat"
            str_sql_is_soft_cat = "SELECT categorie FROM t_soft_cat WHERE id_soft_cat = " \
                                  "%(value_id_soft_cat)s "
            valeur_select_dictionnaire = {"value_id_soft_cat": id_soft_cat_update}
            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()
            mybd_curseur.execute(str_sql_is_soft_cat, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()", vu qu'il n'y a qu'un seul champ "nom licence" pour l'UPDATE
            data_nom_licence = mybd_curseur.fetchone()
            print("data_nom_licence ", data_nom_licence, " type ", type(data_nom_licence), " licence ",
                  data_nom_licence["categorie"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "licences_update_wtf.html"
            form_update.categorie.data = str(data_nom_licence["categorie"])

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans licence_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans licence_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")
        flash(f"Erreur dans licence_update_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")
        flash(f"__KeyError dans licence_update_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("licences/licences_update_wtf.html", form_update=form_update)


"""
    Auteur : OM 2021.04.08
    Définition d'une "route" /genre_delete

    Test : ex. cliquer sur le menu "genres" puis cliquer sur le bouton "DELETE" d'un "genre"

    Paramètres : sans

    But : Effacer(delete) un genre qui a été sélectionné dans le formulaire "genres_afficher.html"

    Remarque :  Dans le champ "nom_genre_delete_wtf" du formulaire "genres/genre_delete_wtf.html",
                le contrôle de la saisie est désactivée. On doit simplement cliquer sur "DELETE"
"""


@obj_mon_application.route("/licence_delete", methods=['GET', 'POST'])
def licence_delete_wtf():
    data_attribue_licence_delete = None
    btn_submit_del = None
    # L'utilisateur vient de cliquer sur le bouton "DELETE". Récupère la valeur de "id_soft_cat"
    id_soft_cat_delete = request.values['id_soft_cat_btn_delete_html']

    # Objet formulaire pour effacer le licence sélectionné.
    form_delete = FormWTFDeletelicence()
    try:
        print(" on submit ", form_delete.validate_on_submit())
        if request.method == "POST" and form_delete.validate_on_submit():

            if form_delete.submit_btn_annuler.data:
                return redirect(url_for("licences_afficher", order_by="ASC", id_soft_cat_sel=0))

            if form_delete.submit_btn_conf_del.data:
                # Récupère les données afin d'afficher à nouveau
                # le formulaire "licences/licences_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
                data_attribue_licence_delete = session['data_attribue_licence_delete']
                print("data_attribue_licence_delete ", data_attribue_licence_delete)

                flash(f"Effacer le licence de façon définitive de la BD !!!", "danger")
                # L'utilisateur vient de cliquer sur le bouton de confirmation pour effacer...
                # On affiche le bouton "Effacer licence" qui va irrémédiablement EFFACER le licence
                btn_submit_del = True

            if form_delete.submit_btn_del.data:
                valeur_delete_dictionnaire = {"value_id_soft_cat": id_soft_cat_delete}
                print("valeur_delete_dictionnaire ", valeur_delete_dictionnaire)

                str_sql_delete_licence = """DELETE FROM t_soft_cat WHERE id_soft_cat = %(value_id_soft_cat)s"""

                # str_sql_delete_films_licence = """DELETE FROM t_soft_cat WHERE fk_licence = %(value_id_soft_cat)s"""
                # str_sql_delete_idlicence = """DELETE FROM t_soft_cat WHERE id_soft_cat = %(value_id_soft_cat)s"""
                # Manière brutale d'effacer d'abord la "fk_licence", même si elle n'existe pas dans la "t_soft_cat"
                # Ensuite on peut effacer le licence vu qu'il n'est plus "lié" (INNODB) dans la "t_soft_cat"
                with MaBaseDeDonnee() as mconn_bd:
                    mconn_bd.mabd_execute(str_sql_delete_licence, valeur_delete_dictionnaire)
                    # mconn_bd.mabd_execute(str_sql_delete_films_licence, valeur_delete_dictionnaire)
                    # mconn_bd.mabd_execute(str_sql_delete_idlicence, valeur_delete_dictionnaire)

                flash(f"licence définitivement effacé !!", "success")
                print(f"licence définitivement effacé !!")

                # afficher les données
                return redirect(url_for('licences_afficher', order_by="ASC", id_soft_cat_sel=0))

        if request.method == "GET":
            valeur_select_dictionnaire = {"value_id_soft_cat": id_soft_cat_delete}
            print(id_soft_cat_delete, type(id_soft_cat_delete))

            # Requête qui affiche tous les films qui ont le licence que l'utilisateur veut effacer
            # str_sql_licence_delete = """SELECT id_soft_cat, nom_film, id_soft_cat, categorie FROM t_soft_cat
            #                                 INNER JOIN t_film ON t_soft_cat.categorie = id_soft_cat
            #                                 INNER JOIN t_soft_cat ON t_soft_cat.categorie = t_soft_cat.id_soft_cat
            #                                 WHERE fk_licence = %(value_id_soft_cat)s"""

            str_sql_licence_delete = """SELECT * FROM t_soft_cat WHERE id_soft_cat = %(value_id_soft_cat)s"""

            mybd_curseur = MaBaseDeDonnee().connexion_bd.cursor()

            mybd_curseur.execute(str_sql_licence_delete, valeur_select_dictionnaire)
            data_attribue_licence_delete = mybd_curseur.fetchall()
            print("data_attribue_licence_delete...", data_attribue_licence_delete)

            # Nécessaire pour mémoriser les données afin d'afficher à nouveau
            # le formulaire "licences/licences_delete_wtf.html" lorsque le bouton "Etes-vous sur d'effacer ?" est cliqué.
            session['data_attribue_licence_delete'] = data_attribue_licence_delete

            # Opération sur la BD pour récupérer "id_soft_cat" et "categorie" de la "t_soft_cat"
            str_sql_is_soft_cat = "SELECT id_soft_cat, categorie FROM t_soft_cat WHERE id_soft_cat = %(value_id_soft_cat)s"

            mybd_curseur.execute(str_sql_is_soft_cat, valeur_select_dictionnaire)
            # Une seule valeur est suffisante "fetchone()",
            # vu qu'il n'y a qu'un seul champ "nom licence" pour l'action DELETE
            data_nom_licence = mybd_curseur.fetchone()
            print("data_nom_licence ", data_nom_licence, " type ", type(data_nom_licence), " licence Update Price ",
                  data_nom_licence["categorie"])

            # Afficher la valeur sélectionnée dans le champ du formulaire "licences_delete_wtf.html"
            form_delete.categorie.data = data_nom_licence["categorie"]

            # Le bouton pour l'action "DELETE" dans le form. "licences_delete_wtf.html" est caché.
            btn_submit_del = False

    # OM 2020.04.16 ATTENTION à l'ordre des excepts, il est très important de respecter l'ordre.
    except KeyError:
        flash(f"__KeyError dans licence_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")
    except ValueError:
        flash(f"Erreur dans licence_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]}", "danger")
    except (pymysql.err.OperationalError,
            pymysql.ProgrammingError,
            pymysql.InternalError,
            pymysql.err.IntegrityError,
            TypeError) as erreur_gest_genr_crud:
        code, msg = erreur_gest_genr_crud.args
        flash(f"attention : {error_codes.get(code, msg)} {erreur_gest_genr_crud} ", "danger")

        flash(f"Erreur dans licence_delete_wtf : {sys.exc_info()[0]} "
              f"{erreur_gest_genr_crud.args[0]} , "
              f"{erreur_gest_genr_crud}", "danger")

        flash(f"__KeyError dans licence_delete_wtf : {sys.exc_info()[0]} {sys.exc_info()[1]} {sys.exc_info()[2]}",
              "danger")

    return render_template("licences/licences_delete_wtf.html",
                           form_delete=form_delete,
                           btn_submit_del=btn_submit_del)
