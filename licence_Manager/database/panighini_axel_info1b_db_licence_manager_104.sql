-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 13 Juin 2021 à 10:28
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `panighini_axel_info1b_db_licence_manager_104`
--

-- --------------------------------------------------------
-- Détection si une autre base de donnée du même nom existe

DROP DATABASE IF EXISTS copie_panighini_axel_info1b_db_licence_manager_104;

-- Création d'un nouvelle base de donnée

CREATE DATABASE IF NOT EXISTS copie_panighini_axel_info1b_db_licence_manager_104;

-- Utilisation de cette base de donnée

USE copie_panighini_axel_info1b_db_licence_manager_104;
--
-- Structure de la table `t_soft_names`
--

CREATE TABLE `t_soft_names` (
  `id_soft_names` int(11) NOT NULL,
  `soft_names` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_soft_names`
--

INSERT INTO `t_soft_names` (`id_soft_names`, `soft_names`, `description`) VALUES
(1, 'Adobe Creative Suite', 'Logiciel de montage vidéo'),
(2, 'Fl Studio Producer edition', 'Logiciel de production de son'),
(3, 'Sao', 'Logiciel inconnu à mon existence'),
(4, 'Cheops', 'Logiciel de recherche de la police française'),
(5, 'Visual studio 2019', 'Interface pour la programation');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_soft_names`
--
ALTER TABLE `t_soft_names`
  ADD PRIMARY KEY (`id_soft_names`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_soft_names`
--
ALTER TABLE `t_soft_names`
  MODIFY `id_soft_names` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
